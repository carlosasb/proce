/*=======================================================================*
*                                                                        *
* Copyright (C) 2015 Carlos Augusto de Souza Braga                       *
* <CASBraga@Gmail.com>                                                   *
*                                                                        *
*  This program is free software: you can redistribute it and/or modify  *
*  it under the terms of the GNU General Public License as published by  *
*  the Free Software Foundation, either version 3 of the License, or     *
*  (at your option) any later version.                                   *
*                                                                        *
*  This program is distributed in the hope that it will be useful,       *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*  GNU General Public License for more details.                          *
*========================================================================*/

This is the README file for the java project ProcessingIOTest. This is, at the moment,
composed of the Mouse.java class. As the name suggests, This class tests some types of
I/O operations using mouse inputs. At the moment, it uses only button presses, not drag.
This is a very silly class, for the purpose of testing these operations as I learn more 
about Processing.

This is an Eclipse project and can be directly imported to Eclipse.
This project is dependent on the Processing libraries (core.jar to be precise).
The library can be downloaded at "https://processing.org/download/" and imported 
directly into this project for use following the instructions at 
"https://processing.org/tutorials/eclipse/".

TODO:

Keyboard class for the same types of tests of I/O operations.

Test more advanced I/O things using only Processing. I think using any of the libraries
is going to require their own tests.