/**=======================================================================*
 *                                                                        *
 * Copyright (C) 2015 Carlos Augusto de Souza Braga                       *
 * <CASBraga@Gmail.com>                                                   *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *========================================================================* 
 * 
 * Class Mouse
 * Extends PApplet data type from Processing package
 * 
 * Creates an applet and plays with mouse
 * contains
 * 
 * Global variables:
 *     isSelected       => boolean is true if a quadrant is
 *                         selected, false otherwise; default 
 *                         is false
 * 
 * Methods:
 *     private:
 *         onMouseClick => checks for mouse button press
 *         onSelection  => changes the color the selecting 
 *                         rectangle
 *     public:
 *         setup        => sets up the canvas; will be 
 *                         called only once
 *         draw         => draws elements to the canvas; will be 
 *                         called multiple times 
 */

import processing.core.*;

public class Mouse extends PApplet {
	// global variables
	private boolean isSelected = false;
	
	// private methods
	private void onMouseClick(){
		// checks if the button is pressed
		// and is the left button
		if (mousePressed && mouseButton == LEFT){
			isSelected = true;
		} else if (mousePressed && mouseButton == RIGHT) {
			isSelected = false;
		}
	}
	
	private void onSelection(){
        // checks selection of quadrant
		// if selected, changes rectangle
		// to white
		if (isSelected){
			fill(255, 255, 255);
		} else{
			fill(0, 0, 0);
		}
	}
	
	// public methods (event handlers)
	public void setup(){
		// create a canvas of size 400 X 400
		size(400, 400);
		// sets the framerate to 60 fps
		frameRate(60);
	}

	public void draw(){
		// changes the background
		background(126);
        
		// prints mouse position to console
		//println(mouseX, mouseY);
		
		// draws an ellipse at mouse position
		//ellipse(mouseX, mouseY, 50, 50);
		
		// draws a line, only if the mouse is 
		// moving fast
		//line(mouseX, mouseY, pmouseX, pmouseY);
		
		// selection example
		// selects a quadrant of the canvas 
		if (mouseX > 0 && mouseY > 0) {
	        if (mouseX < width/2 && mouseY < height/2){
	        	noStroke();
	        	onMouseClick();
	        	onSelection();
	        	rect(0, 0, width/2, height/2);
	        } else if (mouseX > width/2 && mouseY < height/2){
	        	noStroke();
	        	onMouseClick();
	        	onSelection();
	        	rect(width/2, 0, width/2, height/2);
	        } else if (mouseX < width/2 && mouseY > height/2){
	        	noStroke();
	        	onMouseClick();
	        	onSelection();
	        	rect(0, height/2, width/2, height/2);
	        } else{
	        	noStroke();
	        	onMouseClick();
	        	onSelection();
	        	rect(width/2, height/2, width/2, height/2);
	        }
		} else{
			isSelected = false;
		}
	}
}
