/**=======================================================================*
 *                                                                        *
 * Copyright (C) 2015 Carlos Augusto de Souza Braga                       *
 * <CASBraga@Gmail.com>                                                   *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *========================================================================* 
 * 
 * Class Keyboard
 * Extends PApplet data type from Processing package
 * 
 * Creates an applet and plays with keyboard
 * contains
 * 
 * Methods:
 *     public:
 *         setup        => sets up the canvas; will be 
 *                         called only once
 *         draw         => draws elements to the canvas; will be 
 *                         called multiple times
 *     private:
 *         callTime     => integer containing the call instant 
 *                         of getText
 *         tempStr      => temporary storage for user input
 *         userInput    => string containing the user input
 *         getText      => gets user typed text and adds it to a 
 *                         string
 */

import processing.core.*;

public class Keyboard extends PApplet {
	
	// private variables
	private StringBuilder tempStr = new StringBuilder();
	private String userInput = new String();
	private int callTime = 0;
	
	// private methods
	private void getText(){
		// local variables
		int m = millis();
		int repeat = m - callTime;
		String mem = new String(tempStr.toString());
		
		// checks for key pressed event
		if (callTime == 0 || repeat >= 100) {
			if (keyPressed && key != BACKSPACE) {
				mem = mem + key;
				tempStr = new StringBuilder(mem);
				callTime = m;
			}
		}
		
		// prints temporary string to screen 
		text(tempStr.toString(), 0, height / 2);
	}

	// public methods
	public void setup(){
		// create a canvas of size 400 X 400
		size(400, 400);
		// sets the framerate to 60 fps
		frameRate(60);
		// set the stroke to 4 pixels
		strokeWeight(4);
		// text size set to 60 pixels
		textSize(20);
	}
	
	public void draw(){
		// set the background to a 
		// shade of gray
		background(204);
		
		// get text input from user
		getText();
		
		// prints user input to screen
		text("New user input:", 0, height / 7);
		if (userInput.length() > 0) {
			text(userInput, 0, height / 4);
		}
		
		// DEBUGGING prints userInput variable to the console
		//System.out.println(keyPressed);
		
	}
	
	public void keyPressed(){
		// local variables
		int m = millis();
		int repeat = m - callTime;
		
		if (key == BACKSPACE && tempStr.length() > 0){
			if (repeat >= 100){
				tempStr.deleteCharAt(tempStr.length()-1);
			}
		}
	}
	
	public void keyReleased(){
		if (key == ENTER){
			userInput = tempStr.toString();
			tempStr = new StringBuilder("");
			// DEBUGGING
			//System.out.println("Enter key was pressed and released");
		}
	}
}
